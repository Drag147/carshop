name := """CarShop"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(guice,
  javaJdbc,
  evolutions,
  "org.mybatis" % "mybatis" % "3.5.6",
  "org.mybatis" % "mybatis-guice" % "3.12",
  "com.h2database" % "h2" % "1.4.200"
)