-- cars schema

-- !Ups
create table car_model (
    id serial primary key,
    name varchar(255) not null,
    production_year_start integer not null,
    production_year_end integer not null
);

create table car_brand (
    id serial primary key,
    name varchar(255) not null,
    country varchar(255) not null
);

create table car (
    id serial primary key,
    mileage integer not null,
    price float not null,
    production_year integer not null,
    model_id integer references car_model(id) on delete set null,
    brand_id integer references car_brand(id) on delete set null
);

select * from car_model where name like '%Модель%';

-- !Downs
DROP TABLE car;
DROP TABLE car_model;
DROP TABLE car_brand;