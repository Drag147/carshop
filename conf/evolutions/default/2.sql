-- start data

-- !Ups
insert into car_model (name, production_year_start, production_year_end) values
       ('Приора', 2007, 2018),
       ('Passat CC', 2008, 2016);

insert into car_brand (name, country) values
       ('Лада (ВАЗ)', 'Россия'),
       ('Volkswagen', 'Германия');

insert into car (mileage, price, production_year, model_id, brand_id) values
       (123587, 150000, 2012, 1, 1),
       (87458, 980000, 2014, 2, 2);

-- !Downs
delete from car_model;
delete from car_brand;
delete from car;
