package models.domain;

import java.util.Objects;

/**
 * Марка
 */
public class CarBrand {
    /**
     * ID записи в БД
     * для простоты сделал int, можно использовать UUID.
     */
    private Integer id;

    /**
     * Наименование
     */
    private String name;
    /**
     * Страна
     */
    private String country;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarBrand carBrand = (CarBrand) o;
        return Objects.equals(name, carBrand.name) && Objects.equals(country, carBrand.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country);
    }

    /**
     * Метод, для проставления id при получении модели с UI для фронта
     * @param id ID записи
     * @return Тотже объект с установленным ID
     */
    public static CarBrand brandWithId(Integer id) {
        CarBrand carBrand = new CarBrand();
        carBrand.id = id;
        return carBrand;
    }
}
