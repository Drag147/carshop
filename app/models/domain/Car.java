package models.domain;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Машина
 */
public class Car {
    /**
     * ID записи в БД
     * для простоты сделал int, можно использовать UUID.
     */
    private Integer id;
    /**
     * Марка
     */
    private CarBrand brand;
    /**
     * Модель
     */
    private CarModel model;
    /**
     * Год выпуска
     */
    private Integer productionYear;
    /**
     * Пробег
     */
    private int mileage;

    /**
     * Цена
     */
    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public CarBrand getBrand() {
        return brand;
    }

    public void setBrand(CarBrand brand) {
        this.brand = brand;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Integer productionDate) {
        this.productionYear = productionDate;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return mileage == car.mileage && Objects.equals(brand, car.brand) && Objects.equals(model, car.model) &&
            Objects.equals(productionYear, car.productionYear) && Objects.equals(price, car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, productionYear, mileage, price);
    }

    /**
     * Метод, для проставления id при получении модели с UI для фронта
     * @param id ID записи
     * @return Тотже объект с установленным ID
     */
    public Car applyIdForUpdate(Integer id) {
        this.id = id;
        return this;
    }
}
