package models.domain;

import java.util.Objects;

/**
 * Модель
 */
public class CarModel {
    /**
     * ID записи в БД
     * для простоты сделал int, можно использовать UUID.
     */
    private Integer id;
    /**
     * Наименование
     */
    private String name;
    /**
     * Год начала производства
     */
    private Integer productionYearStart;
    /**
     * Год окончания производства
     */
    private Integer productionYearEnd;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductionYearStart() {
        return productionYearStart;
    }

    public void setProductionYearStart(Integer productionYearStart) {
        this.productionYearStart = productionYearStart;
    }

    public Integer getProductionYearEnd() {
        return productionYearEnd;
    }

    public void setProductionYearEnd(Integer productionYearEnd) {
        this.productionYearEnd = productionYearEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarModel carModel = (CarModel) o;
        return Objects.equals(name, carModel.name) &&
            Objects.equals(productionYearStart, carModel.productionYearStart) &&
            Objects.equals(productionYearEnd, carModel.productionYearEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, productionYearStart, productionYearEnd);
    }

    /**
     * Метод, для проставления id при получении модели с UI для фронта
     * @param id ID записи
     * @return Тотже объект с установленным ID
     */
    public static CarModel modelWithId(Integer id) {
        CarModel carModel = new CarModel();
        carModel.id = id;
        return carModel;
    }
}
