package models.filters;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Фильтр по полям машины
 */
public class CarFilter {
    /**
     * Марка
     */
    public Integer brandId;
    /**
     * Модель
     */
    public Integer modelId;
    /**
     * Год выпуска
     */
    public Integer productionYear;
    /**
     * Пробег
     */
    public Integer mileage;
    /**
     * Цена
     */
    public BigDecimal price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarFilter carFilter = (CarFilter) o;
        return Objects.equals(brandId, carFilter.brandId) && Objects.equals(modelId, carFilter.modelId) &&
            Objects.equals(productionYear, carFilter.productionYear) && Objects.equals(mileage, carFilter.mileage) &&
            Objects.equals(price, carFilter.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brandId, modelId, productionYear, mileage, price);
    }
}
