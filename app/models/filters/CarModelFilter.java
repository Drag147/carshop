package models.filters;

import java.util.Objects;

public class CarModelFilter {
    /**
     * Наименование
     */
    public String name;
    /**
     * Год начала производства
     */
    public Integer productionYearStart;
    /**
     * Год окончания производства
     */
    public Integer productionYearEnd;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarModelFilter that = (CarModelFilter) o;
        return Objects.equals(name, that.name) && Objects.equals(productionYearStart, that.productionYearStart) &&
            Objects.equals(productionYearEnd, that.productionYearEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, productionYearStart, productionYearEnd);
    }
}
