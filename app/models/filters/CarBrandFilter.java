package models.filters;

import java.util.Objects;

/**
 * Фильтр по полям марки
 */
public class CarBrandFilter {
    /**
     * Наименование
     */
    public String name;
    /**
     * Страна
     */
    public String country;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarBrandFilter that = (CarBrandFilter) o;
        return Objects.equals(name, that.name) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country);
    }
}
