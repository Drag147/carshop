package controllers;

import javax.inject.Inject;

import models.domain.CarBrand;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarBrandService;
import services.ForkJoinService;
import views.html.carBrands.index;

import static play.libs.Scala.asScala;

public class CarBrandsController extends Controller {

    private final ForkJoinService forkJoinService;
    private final CarBrandService carBrandService;
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;

    @Inject
    public CarBrandsController(ForkJoinService forkJoinService, CarBrandService carBrandService,
        FormFactory formFactory, MessagesApi messagesApi) {
        this.forkJoinService = forkJoinService;
        this.carBrandService = carBrandService;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
    }

    public Result findAll(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            Form<CarBrand> form = formFactory.form(CarBrand.class);

            return ok(index.render(form, asScala(carBrandService.getAll()), request, messagesApi.preferred(request)));
        });
    }

    public Result findById(Http.Request request, Integer id) {
        return forkJoinService.invokeForController(() -> {
            Form<CarBrand> form = formFactory.form(CarBrand.class);
            return ok(index.render(form, asScala(carBrandService.getAll()), request, messagesApi.preferred(request)));
        });
    }

    public Result save(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            Form<CarBrand> carBrandForm = formFactory.form(CarBrand.class).bindFromRequest(request);
            carBrandService.insert(carBrandForm.get());
            return redirect(routes.CarBrandsController.findAll());
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForController(() -> {
            carBrandService.delete(id);
            return ok();
        });
    }
}
