package controllers;

import javax.inject.Inject;

import models.domain.CarModel;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarModelService;
import services.ForkJoinService;
import views.html.carModels.index;

import static play.libs.Scala.asScala;

public class CarModelsController extends Controller {
    private final ForkJoinService forkJoinService;
    private final CarModelService carModelService;
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;

    @Inject
    public CarModelsController(ForkJoinService forkJoinService, CarModelService carModelService,
        FormFactory formFactory, MessagesApi messagesApi) {
        this.forkJoinService = forkJoinService;
        this.carModelService = carModelService;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
    }

    public Result findAll(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            Form<CarModel> form = formFactory.form(CarModel.class);

            return ok(index.render(form, asScala(carModelService.getAll()), request, messagesApi.preferred(request)));
        });
    }

    public Result findById(Http.Request request, Integer id) {
        return forkJoinService.invokeForController(() -> {
            Form<CarModel> form = formFactory.form(CarModel.class);

            return ok(index.render(form, asScala(carModelService.getAll()), request, messagesApi.preferred(request)));
        });
    }

    public Result save(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            Form<CarModel> carModelForm = formFactory.form(CarModel.class).bindFromRequest(request);
            carModelService.insert(carModelForm.get());
            return redirect(routes.CarModelsController.findAll());
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForController(() -> {
            carModelService.delete(id);
            return ok();
        });
    }
}
