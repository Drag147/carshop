package controllers;

import java.util.Map;
import java.util.concurrent.ForkJoinTask;

import javax.inject.Inject;

import models.domain.Car;
import models.domain.CarBrand;
import models.domain.CarModel;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarBrandService;
import services.CarModelService;
import services.CarService;
import services.ForkJoinService;
import views.html.cars.create;
import views.html.cars.edit;
import views.html.cars.index;
import views.html.cars.show;

import static play.libs.Scala.asScala;

public class CarsController extends Controller {
    private final ForkJoinService forkJoinService;
    private final CarService carService;
    private final CarModelService carModelService;
    private final CarBrandService carBrandService;
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;

    @Inject
    public CarsController(ForkJoinService forkJoinService, CarService carService, CarModelService carModelService,
        CarBrandService carBrandService, FormFactory formFactory, MessagesApi messagesApi) {
        this.forkJoinService = forkJoinService;
        this.carService = carService;
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
    }

    public Result findAll() {
        return forkJoinService.invokeForController(() -> ok(index.render(asScala(carService.getAll()))));
    }

    public Result findById(Integer id) {
        return forkJoinService.invokeForController(() -> ok(show.render(carService.getById(id))));
    }

    public Result create(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            Form<Car> carForm = formFactory.form(Car.class);
            return ok(create
                .render(carForm, asScala(carBrandService.getAll()), asScala(carModelService.getAll()), request,
                    messagesApi.preferred(request)));
        });
    }

    public Result save(Http.Request request) {
        return forkJoinService.invokeForController(() -> {
            carService.insert(getNewValueCarFromForm(request).join());

            return redirect(routes.CarsController.findAll());
        });
    }

    public Result edit(Http.Request request, Integer id) {
        return forkJoinService.invokeForController(() -> {
            Car byId = carService.getById(id);

            if (byId == null) {
                return notFound();
            }

            return ok(edit.render(formFactory.form(Car.class).fill(byId), asScala(carBrandService.getAll()),
                asScala(carModelService.getAll()), request, messagesApi.preferred(request)));
        });
    }

    public Result update(Http.Request request, Integer id) {
        return forkJoinService.invokeForController(() -> {
            final ForkJoinTask<Car> taskForGetFromDb =
                forkJoinService.invokeForControllerAndReturn(() -> carService.getById(id)).fork();

            final Car newValueCar = getNewValueCarFromForm(request).join();
            final Car fromDb = taskForGetFromDb.join();

            if (fromDb == null || newValueCar == null) {
                return redirect(routes.CarsController.edit(id));
            }

            carService.update(newValueCar.applyIdForUpdate(id));

            return redirect(routes.CarsController.edit(id));
        });
    }

    private ForkJoinTask<Car> getNewValueCarFromForm(Http.Request request) {
        return forkJoinService.invokeForControllerAndReturn(() -> {
            Form<Car> carForm = formFactory.form(Car.class).bindFromRequest(request);

            Map<String, String> inputs = carForm.rawData();

            Integer brand_id = Integer.valueOf(inputs.get("brand_id"));
            Integer model_id = Integer.valueOf(inputs.get("model_id"));

            Car newCar = carForm.get();

            newCar.setModel(CarModel.modelWithId(model_id));
            newCar.setBrand(CarBrand.brandWithId(brand_id));

            return newCar;
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForController(() -> {
            carService.delete(id);
            return ok();
        });
    }
}
