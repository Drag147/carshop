package controllers.api;

import java.util.List;

import javax.inject.Inject;

import models.domain.CarModel;
import models.filters.CarModelFilter;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarModelService;
import services.ForkJoinService;

public class ModelsApiController extends Controller {

    @Inject
    private ForkJoinService forkJoinService;
    @Inject
    private CarModelService modelService;

    public Result findAll(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            List<CarModel> models = modelService.getAllWithFilter(Json.fromJson(request.body().asJson(), CarModelFilter.class));

            if (models.isEmpty()) {
                return noContent();
            }

            return ok(Json.toJson(models));
        });
    }

    public Result findById(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            CarModel byId = modelService.getById(id);

            if (byId == null) {
                return notFound();
            }

            return ok(Json.toJson(byId));
        });
    }

    public Result create(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            CarModel modelFromJson = fromJson(request);

            if (modelFromJson == null) {
                return badRequest("Incorrect json");
            }

            modelService.insert(modelFromJson);

            return created();
        });
    }

    public Result update(Http.Request request, Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            CarModel carFromJson = fromJson(request);

            if (carFromJson == null) {
                return badRequest("Incorrect JSON");
            }
            if (!id.equals(carFromJson.getId())) {
                return badRequest("Ids from JSON model and request not equals");
            }

            modelService.update(carFromJson);

            return ok();
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            if (id == null) {
                return badRequest("Incorrect id value");
            }

            if (modelService.getById(id) == null) {
                return notFound(String.format("Model with id: %d not found", id));
            }

            modelService.delete(id);

            return ok();
        });
    }

    /**
     * Вспомогательный метод для получения модели из запроса
     * @param request запрос
     * @return модель десериализованная из JSON
     */
    private CarModel fromJson(Http.Request request) {
        return Json.fromJson(request.body().asJson(), CarModel.class);
    }
}
