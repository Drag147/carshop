package controllers.api;

import java.util.List;

import javax.inject.Inject;

import models.domain.CarBrand;
import models.filters.CarBrandFilter;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarBrandService;
import services.ForkJoinService;

public class BrandsApiController extends Controller {

    @Inject
    private ForkJoinService forkJoinService;
    @Inject
    private CarBrandService brandService;

    public Result findAll(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            List<CarBrand> brands = brandService.getAllWithFilter(Json.fromJson(request.body().asJson(), CarBrandFilter.class));

            if (brands.isEmpty()) {
                return noContent();
            }

            return ok(Json.toJson(brands));
        });
    }

    public Result findById(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            CarBrand byId = brandService.getById(id);

            if (byId == null) {
                return notFound();
            }

            return ok(Json.toJson(byId));
        });
    }

    public Result create(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            CarBrand modelFromJson = fromJson(request);

            if (modelFromJson == null) {
                return badRequest("Incorrect json");
            }

            brandService.insert(modelFromJson);

            return created();
        });
    }

    public Result update(Http.Request request, Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            CarBrand carFromJson = fromJson(request);

            if (carFromJson == null) {
                return badRequest("Incorrect JSON");
            }
            if (!id.equals(carFromJson.getId())) {
                return badRequest("Ids from JSON model and request not equals");
            }

            brandService.update(carFromJson);

            return ok();
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            if (id == null) {
                return badRequest("Incorrect id value");
            }

            if (brandService.getById(id) == null) {
                return notFound(String.format("Brand with id: %d not found", id));
            }

            brandService.delete(id);

            return ok();
        });
    }

    /**
     * Вспомогательный метод для получения модели из запроса
     * @param request запрос
     * @return модель десериализованная из JSON
     */
    private CarBrand fromJson(Http.Request request) {
        return Json.fromJson(request.body().asJson(), CarBrand.class);
    }
}
