package controllers.api;

import java.util.List;

import javax.inject.Inject;

import models.domain.Car;
import models.filters.CarFilter;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CarService;
import services.ForkJoinService;

public class CarsApiController extends Controller {

    @Inject
    private ForkJoinService forkJoinService;
    @Inject
    private CarService carService;

    public Result findAll(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            List<Car> cars = carService.getAllWithFilter(Json.fromJson(request.body().asJson(), CarFilter.class));

            if (cars.isEmpty()) {
                return noContent();
            }

            return ok(Json.toJson(cars));
        });
    }

    public Result findById(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            Car byId = carService.getById(id);

            if (byId == null) {
                return notFound();
            }

            return ok(Json.toJson(byId));
        });
    }

    public Result create(Http.Request request) {
        return forkJoinService.invokeForApiController(() -> {
            Car modelFromJson = fromJson(request);

            if (modelFromJson == null) {
                return badRequest("Incorrect json");
            }

            carService.insert(modelFromJson);

            return created();
        });
    }

    public Result update(Http.Request request, Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            Car carFromJson = fromJson(request);

            if (carFromJson == null) {
                return badRequest("Incorrect JSON");
            }
            if (!id.equals(carFromJson.getId())) {
                return badRequest("Ids from JSON model and request not equals");
            }

            carService.update(carFromJson);

            return ok();
        });
    }

    public Result delete(Integer id) {
        return forkJoinService.invokeForApiController(() -> {
            if (id == null) {
                return badRequest("Incorrect id value");
            }

            if (carService.getById(id) == null) {
                return notFound(String.format("Car with id: %d not found", id));
            }

            carService.delete(id);

            return ok();
        });
    }

    /**
     * Вспомогательный метод для получения модели из запроса
     * @param request запрос
     * @return модель десериализованная из JSON
     */
    private Car fromJson(Http.Request request) {
        return Json.fromJson(request.body().asJson(), Car.class);
    }
}
