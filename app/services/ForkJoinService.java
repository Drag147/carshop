package services;

import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public interface ForkJoinService {

    /**
     * Метод для настройки пула для контроллеров
     * @return Настроенный пул для контроллеров
     */
    ForkJoinPool configureControllerPool();

    /**
     * Метод для настройки пула для контроллеров API
     * @return Настроенный пул для контроллеров API
     */
    ForkJoinPool configureApiControllerPool();

    /**
     * Метод для настройки пула для сервисов
     * @return Настроенный пул для сервисов
     */
    ForkJoinPool configureServicePool();

    /**
     * Метод для настройки пула для репозиториев
     * @return Настроенный пул для репозиториев
     */
    ForkJoinPool configureRepositoryPool();


    /**
     * Метод для запуска таски контроллера в указанном пуле
     * @param task Таска для запуска
     * @param <Result> Результат
     * @return Результат
     */
    <Result> Result invokeForController(Callable<Result> task);

    /**
     * Метод для запуска таски контроллера в указанном пуле
     * @param task Таска для запуска
     * @param <Result> Результат
     * @return Результат
     */
    <Result> ForkJoinTask<Result> invokeForControllerAndReturn(Callable<Result> task);


    /**
     * Метод для запуска таски контроллера API в указанном пуле
     * @param task Таска для запуска
     * @param <Result> Результат
     * @return Результат
     */
    <Result> Result invokeForApiController(Callable<Result> task);


    /**
     * Метод для запуска таски сервиса в указанном пуле
     * @param task Таска для запуска
     * @param <Result> Результат
     * @return Результат
     */
    <Result> Result invokeForService(Callable<Result> task);

    /**
     * Метод для запуска таски сервиса в указанном пуле
     * @param task Таска для запуска
     */
    void invokeForService(Runnable task);


    /**
     * Метод для запуска таски репозитория в указанном пуле
     * @param task Таска для запуска
     * @param <Result> Результат
     * @return Результат
     */
    <Result> Result invokeForRepository(Callable<Result> task);

    /**
     * Метод для запуска таски репозитория в указанном пуле
     * @param task Таска для запуска
     */
    void invokeForRepository(Runnable task);
}
