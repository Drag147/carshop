package services;

import java.util.List;

import models.domain.Car;
import models.filters.CarFilter;

/**
 * Бизнес сервис для работы с Машинами
 */
public interface CarService {
    /**
     * Получить список всех машин
     * @return Список машин
     */
    List<Car> getAll();

    /**
     * Получить список отфильтрованный список машин
     * @return Список машин
     */
    List<Car> getAllWithFilter(CarFilter filter);

    /**
     * Получить конкретную машину по ID
     * @param id ID машины
     * @return Конкретная машина если такая найдена, иначе null
     */
    Car getById(Integer id);

    /**
     * Добавить новую машину
     * @param newValue Информация о новой машине
     */
    void insert(Car newValue);

    /**
     * Обновить информацию о машине
     * @param newValue Новая информация о машине
     */
    void update(Car newValue);

    /**
     * Удалить машину
     * @param id ID машины для удаления
     */
    void delete(Integer id);
}
