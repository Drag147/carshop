package services;

import java.util.List;

import models.domain.CarModel;
import models.filters.CarModelFilter;

/**
 * Бизнес сервис по работе с Моделями машин
 */
public interface CarModelService {
    /**
     * Получить список всех моделей машин
     * @return Лист всех моеделей машин
     */
    List<CarModel> getAll();
    /**
     * Получить список моделей машин по фильтру
     * @return Лист моеделей машин
     */
    List<CarModel> getAllWithFilter(CarModelFilter filter);

    /**
     * Получить модель машины по ID
     * @param id ID модели машины
     * @return Найденная модель машины, иначе null
     */
    CarModel getById(Integer id);

    /**
     * Добавить новую модель машины
     * @param newValue Информация о новой модели машины
     */
    void insert(CarModel newValue);

    /**
     * Обновить модель машины
     * @param newValue Новая информаци о модели машины
     */
    void update(CarModel newValue);

    /**
     * Удалить модель машины с указанным ID
     * @param id ID удаляемой модели машины
     */
    void delete(Integer id);
}
