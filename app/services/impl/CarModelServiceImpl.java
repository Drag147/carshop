package services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import models.domain.CarModel;
import models.filters.CarModelFilter;
import repositories.FilterRepository;
import services.CarModelService;
import services.ForkJoinService;

@Singleton
public class CarModelServiceImpl implements CarModelService {

    private final ForkJoinService forkJoinService;
    private final FilterRepository<CarModel, CarModelFilter, Integer> repository;

    @Inject
    public CarModelServiceImpl(ForkJoinService forkJoinService,
        FilterRepository<CarModel, CarModelFilter, Integer> repository) {
        this.forkJoinService = forkJoinService;
        this.repository = repository;
    }

    @Override
    public List<CarModel> getAll() {
        return forkJoinService.invokeForService(() -> repository.findAll());
    }

    @Override
    public List<CarModel> getAllWithFilter(CarModelFilter filter) {
        return forkJoinService.invokeForService(() -> {
            if (filter == null) {
                return this.getAll();
            }
            return repository.findWithFilter(filter);
        });
    }

    @Override
    public CarModel getById(Integer id) {
        return forkJoinService.invokeForService(() -> repository.findById(id));
    }

    @Override
    public void insert(CarModel newValue) {
        forkJoinService.invokeForService(() -> repository.insert(newValue));
    }

    @Override
    public void update(CarModel newValue) {
        forkJoinService.invokeForService(() -> repository.update(newValue));
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForService(() -> repository.delete(id));
    }
}
