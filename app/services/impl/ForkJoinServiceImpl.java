package services.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import javax.inject.Singleton;

import services.ForkJoinService;

@Singleton
public class ForkJoinServiceImpl implements ForkJoinService {

    private final ForkJoinPool controllerPool;
    private final ForkJoinPool apiControllerPool;
    private final ForkJoinPool servicePool;
    private final ForkJoinPool repositoryPool;

    public ForkJoinServiceImpl() {
        controllerPool = configureControllerPool();
        apiControllerPool = configureApiControllerPool();
        servicePool = configureControllerPool();
        repositoryPool = configureRepositoryPool();
    }

    @Override
    public ForkJoinPool configureControllerPool() {
        return new ForkJoinPool(Runtime.getRuntime().availableProcessors());
    }
    @Override
    public ForkJoinPool configureApiControllerPool() {
        return new ForkJoinPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public ForkJoinPool configureServicePool() {
        return new ForkJoinPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public ForkJoinPool configureRepositoryPool() {
        return new ForkJoinPool(Runtime.getRuntime().availableProcessors());
    }


    @Override
    public <Result> Result invokeForController(Callable<Result> task) {
        return controllerPool.submit(task).join();
    }

    @Override
    public <Result> ForkJoinTask<Result> invokeForControllerAndReturn(Callable<Result> task) {
        return controllerPool.submit(task);
    }


    @Override
    public <Result> Result invokeForApiController(Callable<Result> task) {
        return apiControllerPool.submit(task).join();
    }


    @Override
    public <Result> Result invokeForService(Callable<Result> task) {
        return servicePool.submit(task).join();
    }

    @Override
    public void invokeForService(Runnable task) {
        servicePool.submit(task);
    }


    @Override
    public <Result> Result invokeForRepository(Callable<Result> task) {
        return repositoryPool.submit(task).join();
    }

    @Override
    public void invokeForRepository(Runnable task) {
        repositoryPool.submit(task).join();
    }
}
