package services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import models.domain.CarBrand;
import models.filters.CarBrandFilter;
import repositories.FilterRepository;
import services.CarBrandService;
import services.ForkJoinService;

@Singleton
public class CarBrandServiceImpl implements CarBrandService {

    private final ForkJoinService forkJoinService;
    private final FilterRepository<CarBrand, CarBrandFilter, Integer> repository;

    @Inject
    public CarBrandServiceImpl(ForkJoinService forkJoinService,
        FilterRepository<CarBrand, CarBrandFilter, Integer> repository) {
        this.forkJoinService = forkJoinService;
        this.repository = repository;
    }

    @Override
    public List<CarBrand> getAll() {
        return forkJoinService.invokeForService(repository::findAll);
    }

    @Override
    public List<CarBrand> getAllWithFilter(CarBrandFilter filter) {
        return forkJoinService.invokeForService(() -> {
            if (filter == null) {
                return this.getAll();
            }
            return repository.findWithFilter(filter);
        });
    }

    @Override
    public CarBrand getById(Integer id) {
        return forkJoinService.invokeForService(() -> repository.findById(id));
    }

    @Override
    public void insert(CarBrand newValue) {
        forkJoinService.invokeForService(() -> repository.insert(newValue));
    }

    @Override
    public void update(CarBrand newValue) {
        forkJoinService.invokeForService(() -> repository.update(newValue));
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForService(() -> repository.delete(id));
    }
}
