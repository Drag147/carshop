package services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import models.domain.Car;
import models.filters.CarFilter;
import repositories.FilterRepository;
import services.CarService;
import services.ForkJoinService;

@Singleton
public class CarServiceImpl implements CarService {

    private final ForkJoinService forkJoinService;
    private final FilterRepository<Car, CarFilter, Integer> repository;

    @Inject
    public CarServiceImpl(ForkJoinService forkJoinService, FilterRepository<Car, CarFilter, Integer> repository) {
        this.forkJoinService = forkJoinService;
        this.repository = repository;
    }

    @Override
    public List<Car> getAll() {
        return forkJoinService.invokeForService(() -> repository.findAll());
    }

    @Override
    public List<Car> getAllWithFilter(CarFilter filter) {
        return forkJoinService.invokeForService(() -> {
            if (filter == null) {
                return this.getAll();
            }
            return repository.findWithFilter(filter);
        });
    }

    @Override
    public Car getById(Integer id) {
        return forkJoinService.invokeForService(() -> repository.findById(id));
    }

    @Override
    public void insert(Car newValue) {
        forkJoinService.invokeForService(() -> repository.insert(newValue));
    }

    @Override
    public void update(Car newValue) {
        forkJoinService.invokeForService(() -> repository.update(newValue));
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForService(() -> repository.delete(id));
    }
}
