package services;

import java.util.List;

import models.domain.CarBrand;
import models.filters.CarBrandFilter;

/**
 * Бизнес сервис по работе с Марками машин
 */
public interface CarBrandService {
    /**
     * Получить список всех марок машин
     * @return Лист всех марок машин
     */
    List<CarBrand> getAll();
    /**
     * Получить список отфильтрованных марок машин
     * @return Лист марок машин
     */
    List<CarBrand> getAllWithFilter(CarBrandFilter filter);

    /**
     * Получить марку машины по ID
     * @param id ID марки машины
     * @return Найденная марка машины, иначе null
     */
    CarBrand getById(Integer id);

    /**
     * Добавить новую марку машины
     * @param newValue Информация о новой марке машины
     */
    void insert(CarBrand newValue);

    /**
     * Обновить марку машины
     * @param newValue Новая информаци о марке машины
     */
    void update(CarBrand newValue);

    /**
     * Удалить марку машины с указанным ID
     * @param id ID удаляемой марки машины
     */
    void delete(Integer id);
}
