package modules;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.sql.DataSource;

import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import models.domain.Car;
import models.domain.CarBrand;
import models.domain.CarModel;
import models.filters.CarBrandFilter;
import models.filters.CarFilter;
import models.filters.CarModelFilter;
import play.db.Database;
import repositories.FilterRepository;
import repositories.mappers.CarBrandMapper;
import repositories.mappers.CarMapper;
import repositories.mappers.CarModelMapper;
import repositories.mybatis.CarBrandRepositoryMyBatis;
import repositories.mybatis.CarModelRepositoryMyBatis;
import repositories.mybatis.CarRepositoryMyBatis;
import services.CarBrandService;
import services.CarModelService;
import services.CarService;
import services.ForkJoinService;
import services.impl.CarBrandServiceImpl;
import services.impl.CarModelServiceImpl;
import services.impl.CarServiceImpl;
import services.impl.ForkJoinServiceImpl;

public class MyBatisModule extends org.mybatis.guice.MyBatisModule {

    @Override
    protected void initialize() {
        environmentId("development");
        bindConstant().annotatedWith(
            Names.named("mybatis.configuration.failFast")).
            to(true);

        bindDataSourceProviderType(PlayDataSourceProvider.class);
        bindTransactionFactoryType(JdbcTransactionFactory.class);

        addMapperClass(CarMapper.class);
        addMapperClass(CarModelMapper.class);
        addMapperClass(CarBrandMapper.class);

        bind(ForkJoinService.class).to(ForkJoinServiceImpl.class).in(Scopes.SINGLETON);

        bind(CarService.class).to(CarServiceImpl.class).in(Scopes.SINGLETON);
        bind(CarBrandService.class).to(CarBrandServiceImpl.class).in(Scopes.SINGLETON);
        bind(CarModelService.class).to(CarModelServiceImpl.class).in(Scopes.SINGLETON);

        bind(new TypeLiteral<FilterRepository<Car, CarFilter, Integer>>(){})
            .to(new TypeLiteral<CarRepositoryMyBatis>(){})
            .in(Scopes.SINGLETON);

        bind(new TypeLiteral<FilterRepository<CarBrand, CarBrandFilter, Integer>>(){})
            .to(new TypeLiteral<CarBrandRepositoryMyBatis>(){})
            .in(Scopes.SINGLETON);

        bind(new TypeLiteral<FilterRepository<CarModel, CarModelFilter, Integer>>(){})
            .to(new TypeLiteral<CarModelRepositoryMyBatis>(){})
            .in(Scopes.SINGLETON);
    }

    /* Provides a {@link DataSource} from the {@link Database} which can be injected from Play. */
    @Singleton
    public static class PlayDataSourceProvider implements Provider<DataSource> {
        final Database db;

        @Inject
        public PlayDataSourceProvider(final Database db) {
            this.db = db;
        }


        @Override
        public DataSource get() {
            return db.getDataSource();
        }
    }
}