package repositories.mappers;

import java.util.List;

import models.domain.CarModel;
import models.filters.CarModelFilter;

public interface CarModelMapper {
    List<CarModel> findAll();
    List<CarModel> findWithFilter(CarModelFilter filter);
    CarModel findById(Integer id);
    void insert(CarModel newCarModel);
    void update(CarModel newCarModel);
    void delete(Integer id);
}
