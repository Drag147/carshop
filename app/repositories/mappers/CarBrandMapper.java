package repositories.mappers;

import java.util.List;

import models.domain.CarBrand;
import models.filters.CarBrandFilter;

public interface CarBrandMapper {
    List<CarBrand> findAll();
    List<CarBrand> findWithFilter(CarBrandFilter filter);
    CarBrand findById(Integer id);
    void insert(CarBrand newCarModel);
    void update(CarBrand newCarModel);
    void delete(Integer id);
}
