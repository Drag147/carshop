package repositories.mappers;

import java.util.List;

import models.domain.Car;
import models.filters.CarFilter;

public interface CarMapper {
    List<Car> findAll();
    List<Car> findWithFilter(CarFilter filter);
    Car findById(Integer id);
    void insert(Car newCar);
    void update(Car newCar);
    void delete(Integer id);
}
