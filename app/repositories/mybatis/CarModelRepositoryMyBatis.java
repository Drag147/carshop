package repositories.mybatis;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import models.domain.CarModel;
import models.filters.CarModelFilter;
import repositories.FilterRepository;
import repositories.mappers.CarModelMapper;
import services.ForkJoinService;

@Singleton
public class CarModelRepositoryMyBatis implements FilterRepository<CarModel, CarModelFilter, Integer> {

    private final ForkJoinService forkJoinService;
    private final SqlSessionFactory sqlSessionFactory;

    @Inject
    public CarModelRepositoryMyBatis(ForkJoinService forkJoinService, SqlSessionFactory sqlSessionFactory) {
        this.forkJoinService = forkJoinService;
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public List<CarModel> findAll() {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<CarModel> result = sqlSession.getMapper(CarModelMapper.class).findAll();
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public CarModel findById(Integer id) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final CarModel result = sqlSession.getMapper(CarModelMapper.class).findById(id);
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public void insert(CarModel newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarModelMapper.class).insert(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void update(CarModel newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarModelMapper.class).update(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarModelMapper.class).delete(id);
                sqlSession.commit();
            }
        });
    }

    @Override
    public List<CarModel> findWithFilter(CarModelFilter filter) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<CarModel> result = sqlSession.getMapper(CarModelMapper.class).findWithFilter(filter);
                sqlSession.commit();
                return result;
            }
        });
    }
}
