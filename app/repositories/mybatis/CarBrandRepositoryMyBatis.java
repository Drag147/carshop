package repositories.mybatis;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import models.domain.CarBrand;
import models.filters.CarBrandFilter;
import repositories.FilterRepository;
import repositories.mappers.CarBrandMapper;
import services.ForkJoinService;

@Singleton
public class CarBrandRepositoryMyBatis implements FilterRepository<CarBrand, CarBrandFilter, Integer> {

    private final ForkJoinService forkJoinService;
    private final SqlSessionFactory sqlSessionFactory;

    @Inject
    public CarBrandRepositoryMyBatis(ForkJoinService forkJoinService, SqlSessionFactory sqlSessionFactory) {
        this.forkJoinService = forkJoinService;
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public List<CarBrand> findAll() {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<CarBrand> result = sqlSession.getMapper(CarBrandMapper.class).findAll();
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public CarBrand findById(Integer id) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final CarBrand result = sqlSession.getMapper(CarBrandMapper.class).findById(id);
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public void insert(CarBrand newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarBrandMapper.class).insert(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void update(CarBrand newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarBrandMapper.class).update(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarBrandMapper.class).delete(id);
                sqlSession.commit();
            }
        });
    }

    @Override
    public List<CarBrand> findWithFilter(CarBrandFilter filter) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<CarBrand> result = sqlSession.getMapper(CarBrandMapper.class).findWithFilter(filter);
                sqlSession.commit();
                return result;
            }
        });
    }
}
