package repositories.mybatis;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import models.domain.Car;
import models.filters.CarFilter;
import repositories.FilterRepository;
import repositories.mappers.CarMapper;
import services.ForkJoinService;

@Singleton
public class CarRepositoryMyBatis implements FilterRepository<Car, CarFilter, Integer> {

    private final ForkJoinService forkJoinService;
    private final SqlSessionFactory sqlSessionFactory;

    @Inject
    public CarRepositoryMyBatis(ForkJoinService forkJoinService, SqlSessionFactory sqlSessionFactory) {
        this.forkJoinService = forkJoinService;
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public List<Car> findAll() {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<Car> result = sqlSession.getMapper(CarMapper.class).findAll();
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public Car findById(Integer id) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final Car result = sqlSession.getMapper(CarMapper.class).findById(id);
                sqlSession.commit();
                return result;
            }
        });
    }

    @Override
    public void insert(Car newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarMapper.class).insert(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void update(Car newValue) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarMapper.class).update(newValue);
                sqlSession.commit();
            }
        });
    }

    @Override
    public void delete(Integer id) {
        forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.getMapper(CarMapper.class).delete(id);
                sqlSession.commit();
            }
        });
    }

    @Override
    public List<Car> findWithFilter(CarFilter filter) {
        return forkJoinService.invokeForRepository(() -> {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                final List<Car> result = sqlSession.getMapper(CarMapper.class).findWithFilter(filter);
                sqlSession.commit();
                return result;
            }
        });
    }
}
