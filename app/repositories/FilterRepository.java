package repositories;

import java.util.List;

public interface FilterRepository<Model, FilterModel, IdType> extends CrudRepository<Model, IdType> {
    /**
     * Метод для получения записей с указанным фильтром
     * @return лист найденных записей
     */
    List<Model> findWithFilter(FilterModel filter);
}
