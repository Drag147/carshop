package repositories;

import java.util.List;

/**
 * Интерфейс для CRUD репозитория
 * @param <Model> Тип модели
 * @param <IdType> Тип id
 */
public interface CrudRepository<Model, IdType> {
    /**
     * Метод для получения всех записей
     * @return лист записей
     */
    List<Model> findAll();

    /**
     * Метод для получения записи по ID
     * @param id ID записи
     * @return Найденную запись с указанным ID, иначе null.
     */
    Model findById(IdType id);

    /**
     * Создание новой записи
     * @param newValue Создаваемая запись
     */
    void insert(Model newValue);

    /**
     * Обновление записи
     * @param newValue Новые значения записи
     */
    void update(Model newValue);

    /**
     * Удаление записи по ID
     * @param id ID записи
     */
    void delete(IdType id);
}
